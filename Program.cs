﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using System.Drawing;

namespace EXPEDIENTE
{
    class Program
    {
        static void Main(string[] args)
        {
            //cogito ergo sum
            //Ultimo cambio
            //10/15/2
            //"Abandonar toda esperanza, quienes aquí entráis"


            #region Variables

            string rutoriginal = ConfigurationManager.AppSettings["rutaoriginal"].ToString();
            DirectoryInfo Rutoriginal = new DirectoryInfo(rutoriginal);
            string rutcomplementario = ConfigurationManager.AppSettings["rutacomplementario"].ToString();
            DirectoryInfo Rutcomplementario = new DirectoryInfo(rutcomplementario);
            string rutaduanet = ConfigurationManager.AppSettings["rutaaduanet"].ToString();
            string Aduanas = ConfigurationManager.AppSettings["aduanas"].ToString();
            string[] AduanasArray;
            AduanasArray = Aduanas.Split(@",".ToCharArray());
            string numped = "";
            string aduana = "";
            string patente = "";
            string file = "";          
            DateTime fecha = DateTime.Now;

            //Instancia
            digital objt = new digital();
            #endregion

            Console.WriteLine("START: " + fecha);
            try
            {

                #region Proceso de archivos originales
                gitConsole.WriteLine("SE INICIA PROCESO DE ARCHIVOS ZIP");
                foreach (var files in Rutoriginal.GetFiles("*"))
                {
                    string sfile = files.ToString();
                    FileInfo ffile = new FileInfo(rutoriginal + @"\" + sfile);
                    var lfile = ffile.Length;
                    if (file.Length == 17 || sfile.Length == 18 || sfile.Length == 22 || sfile.Length == 28)
                    {
                        if (lfile != 0)
                        {
                            aduana = sfile.Substring(11, 3);
                            numped = sfile.Substring(4, 7);
                            patente = sfile.Substring(0, 4);
                            objt.process(aduana, numped, patente, sfile);
                        }
                        else
                        {
                            Console.WriteLine("El archivo esta da;ado");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Es un ZIP o RAR que no es de Aduanet");
                        objt.Log(sfile, "ERR", "Es un ZIP o RAR que no es de Aduanet", "GENERAL");
                    }

                }
                Console.WriteLine("SE FINALIZA PROCESO DE ARCHIVOS COMPLEMENTARIOS");
                #endregion

                #region Nuevo Proceso de FTP Aduanet                
                //for (int i = 0; i < AduanasArray.Length; i++)
                //{
                //    Console.WriteLine("SE INICIO PROCESO DEL FTP");
                //    objt.aduanet(rutaduanet + @"\" + AduanasArray[i]);
                //    Console.WriteLine("SE FINALIZO PROCESO DE ADUANA " + AduanasArray[i]);
                //    objt.aduanetstatus(rutaduanet + @"\" + AduanasArray[i]);
                //}
                #endregion

                #region Proceso de archivos complementarios
                Console.WriteLine("SE INICIA PROCESO DE ARCHIVOS COMPLEMENTARIOS");
                foreach (var files in Rutcomplementario.GetFiles("*"))
                {
                    file = Convert.ToString(files);
                    string sfile = files.ToString();
                    FileInfo ffile = new FileInfo(rutcomplementario + @"\" + sfile);
                    var lfile = ffile.Length;

                    if (sfile.Contains(".pdf"))
                    {
                        if (sfile.Length == 11)
                        {

                            if (lfile != 0)
                            {
                                numped = sfile.Substring(0, 7);
                                objt.process("240", numped, "3649", sfile);
                            }
                            else
                            {
                                Console.WriteLine("El archivo esta da;ado");
                            }
                        }
                        else if (sfile.Length == 12)
                        {
                            if (lfile != 0)
                            {
                                numped = sfile.Substring(0, 7);
                                objt.process("240", numped,"3649", sfile);
                            }
                            else
                            {
                                Console.WriteLine("El archivo esta da;ado");
                            }
                        }
                        else if (sfile.Length > 15)
                        {
                            if (file.Contains("am3.hoja.calculo"))
                            {
                                string rz = "";
                                numped = file.Substring(22, 7);
                                rz = file.Substring(30, 12);
                                if (rz == "BMH120709897")
                                {
                                    objt.process("270", numped, "", file);
                                }
                                else
                                {
                                    objt.process("240", numped, "", file);
                                }
                            }
                            else if (file.Substring(0, 3) == "MV_")
                            {
                                aduana = file.Substring(8, 3);
                                numped = file.Substring(12, 7);
                                objt.process(aduana, numped, "", file);
                            }
                            else
                            {
                                aduana = file.Substring(0, 3);
                                numped = file.Substring(9, 7);
                                objt.process(aduana, numped, "", file);
                            }
                        }
                    }
                    else 
                    {
                        objt.Log(file, "ERR", "FUE MAL ESCANEADO", "GENERAL");
                    }
                }
                Console.WriteLine("SE FINALIZA PROCESO DE ARCHIVOS COMPLEMENTARIOS");
                #endregion

                //Console.WriteLine("PROCESO DE ELIMINACION");
                //objt.delete();
            }
            catch (Exception err)
            {      
                Console.WriteLine("EXCEPTION: " + err);
                objt.Log(err.ToString(), "ERR", "ERROR EN EL MAIN", "GENERAL");
            }
            Console.WriteLine("END: " + fecha);     
        }
    }
}
